# BossaBox - Shared Components Library

Biblioteca de componentes compartilhados da BossaBox. Utilizamos essa biblioteca pra compartilhar componentes que precisariam ser duplicados para visualização em aplicações distintas sob um mesmo contexto (ex: Agenda de Imersão, que pode ser visualizada no tanto back-office quanto na aplicação de customers).

## Instalação

Para instalar o pacote no projeto, basta executar:

```
yarn config set @bossabox-code:registry https://gitlab.com/api/v4/packages/npm/
yarn add @bossabox-code/shared-components
```

## Instalar as dependências de um projeto que tem o pacote de shared-components pela primeira vez

Antes de instalar as dependências de um projeto que tem este pacote como dependência, é necessário executar o seguinte comando:

```
yarn config set @bossabox-code:registry https://gitlab.com/api/v4/packages/npm/

yarn install
```

## Semantic Releases

Utilizamos a biblioteca [semantic-release](https://github.com/semantic-release/semantic-release) para automatizar o versionamento e a publicação de novas versões. Atualmente, as tags que estão habilitadas para fazer release são: `feat, fix, perf, chore, ci, docs, refactor`
Obs: Não é necessário alterar a versão que se encontra no package.json

## Merge Requests

As merge requests abertas devem seguir o seguinte padrão na descrição:

- Nome da história
- Link da história no clickup (se houver)
- Link do figma (se houver)
- Descrição dos arquivos que foram alterados
- Resultados: screenshots, gifs, vídeos

## Testando com alterações locais

Para ser possível testar o pacote realizando alterações locais, utilizamos `yarn link`:

1. Acesse a pasta do dls no seu computador e execute o comando `yarn link --link-folder ~/.links`;
2. Acesse a pasta do projeto que utiliza o dls e execute o comando `yarn link --link-folder ~/.links "@bossabox-code/shared-components"`;

Para finalizar a ligação, basta seguir os seguintes passados:

1. Acesse a pasta do projeto que utiliza o dls e execute o comando `yarn unlink --link-folder ~/.links "@bossabox-code/shared-components"`;
2. Acesse a pasta do dls no seu computador e execute o comando `yarn unlink --link-folder ~/.links`;
3. Volte na pasta do projeto que utiliza o dls e execute `yarn install --force`;
