export { default as BAgendaItemCard } from './components/product-agenda/AgendaItemCard.vue';
export { default as BCommentCard } from './components/comments/CommentCard.vue';
export { default as BListComments } from './components/comments/ListComments.vue';
export { default as BTaskManager } from './components/task-manager/TaskManager.vue';
export { default as BTaskManagerForm } from './components/task-manager/TaskForm.vue';
export { default as BWikiRow } from './components/wiki/WikiRow.vue';
export { default as BWikiCollapse } from './components/wiki/WikiCollapse.vue';
export { default as BWikiAttachmentRow } from './components/wiki/WikiAttachmentRow.vue';
